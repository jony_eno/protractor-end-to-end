var ApplicationKeywordsObj = require("../../pages/ApplicationKeywords.js");
var SetupObj = require("../../pages/Setup");
var ExcelReader = require("../../utilities/ExcelReader");
var CommonObj = require("../../pages/Common");
var HomeObj = require("../../pages/Home");

let firstValue = ExcelReader.getTestData(CommonObj.homeSheet, "FirstValue");
let secondValue = ExcelReader.getTestData(CommonObj.homeSheet, "SecondValue");

describe("Super Calculator Division Operation Functionality Test, ", function () {
    beforeAll(function () {
        SetupObj.setUp();
    });

    it("Verifying division operation of super calculator", function () {
        ApplicationKeywordsObj.setValueIntoSpecificElement(HomeObj.firstNumberInputFieldElement, firstValue);
        ApplicationKeywordsObj.setValueIntoSpecificElement(HomeObj.secondNumberInputFieldElement, secondValue);
        HomeObj.selectDropdownOption(HomeObj.divisionOption);
        ApplicationKeywordsObj.clickOnSpecificElement(HomeObj.goButtonElement);
        HomeObj.verifyCalculatedResult('2');
    });
});
