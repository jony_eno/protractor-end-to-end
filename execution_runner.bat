@echo off
:: Following lines getting the system current data time stamp to create report folder
for /f "delims=" %%a in ('wmic OS Get localdatetime  ^| find "."') do set dt=%%a
set YYYY=%dt:~0,4%
set MM=%dt:~4,2%
set DD=%dt:~6,2%
set HH=%dt:~8,2%
set Min=%dt:~10,2%
set Sec=%dt:~12,2%
set stamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%

:: The first portion of the following line will delete the 'allure-results' directory generated in the previous execution.
:: The second portion will execute the conf.js file.
:: The third portion will generate the allure report in the '../Automation_Protractor/testResult' directory.

call rd /s /q allure-results & protractor conf.js & allure generate allure-results --clean  -o testResults/allure-reports_%stamp% & node .\utilities\SendMail.js
