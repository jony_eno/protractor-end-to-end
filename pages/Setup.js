var ApplicationKeywordsObj = require("./ApplicationKeywords.js");
var ExcelReadObj = require("../utilities/ExcelReader");

var SetupPage = function () {
  // [Start:] Login Page Elements
  this.loginIdInputFieldElement = element(by.xpath("//input[@id='loginid']"));
  this.passwordInputFieldElement = element(by.xpath("//input[@id='pwd']"));
  this.signInButtonElement = element(by.xpath("//a[@id='login']"));
  this.sideMenuBarElement = element(by.xpath("//ul[@id='sideBarMenu']//div[@id='headingTwo']//a"));
  this.angularjsWebsiteElement = element(by.xpath("//strong[text()='AngularJS Website']"));
  // [End:] Login Page Elements

  this.setUp = function () {
    browser.driver.manage().deleteAllCookies();
    browser.get(ExcelReadObj.getConfigurationValue("ConfigValue", "AppURL"));
  };
};

module.exports = new SetupPage();
