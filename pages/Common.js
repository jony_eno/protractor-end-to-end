var CommonPage = function () {
  // [Start]: Test Data Excel File Sheet Names
  this.homeSheet = "Home";
  this.loginSheet = "Login";
  // [End]: Test Data Excel File Sheet Names
};

module.exports = new CommonPage();
