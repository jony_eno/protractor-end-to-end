var ApplicationKeywordsObj = require("../pages/ApplicationKeywords.js");
var HomePage = function () {
  // [Start]: Home page elements
  this.firstNumberInputFieldElement = element(by.model('first'));
  this.secondNumberInputFieldElement = element(by.model('second'));
  this.goButtonElement = element(by.css('[ng-click="doAddition()"]'));
  this.dropdownElement = element(by.model('operator'));
  // [End]: Home page elements

  // [Start]: Dropdown options 
  this.additionOption = "ADDITION";
  this.divisionOption = "DIVISION";
  this.moduloOption = "MODULO";
  this.multiplicationOption = "MULTIPLICATION";
  this.subtractionOption = "SUBTRACTION";
  // [End]: Dropdown options

  this.verifyCalculatedResult = function (result) {
    let output = element(by.cssContainingText('h2.ng-binding', result)).getText();
    ApplicationKeywordsObj.assertEqual(output, result);
  }

  this.selectDropdownOption = function (dropDownOption) {
    ApplicationKeywordsObj.clickOnSpecificElement(this.dropdownElement);
    let optionElement = element(by.css("option[value='" + dropDownOption + "']"));
    ApplicationKeywordsObj.clickOnSpecificElement(optionElement);
    ApplicationKeywordsObj.clickOnSpecificElement(this.firstNumberInputFieldElement);
  }
};

module.exports = new HomePage();
