var ExcelReadObj = require("../utilities/ExcelReader");
let count = 0;

function waitForElementToBeVisible(element, message) {
  var EC = protractor.ExpectedConditions;
  element = browser.wait(EC.invisibilityOf(element), 60000, message);
}

let setValueIntoSpecificElement = function (element, value) {
  element.click();
  element.clear();
  element.sendKeys(value);
};

let clickOnSpecificElement = function (element) {
  element.click();
};

let assertEqual = function (actualResult, expectedResult) {
  expect(actualResult).toEqual(expectedResult, "Both value should be equal.");
};

let selectSpecificDropdownOption = function (element, optionName) {
  browser.executeScript("arguments[0].value='" + optionName + "'", element);
};

module.exports = {
  setValueIntoSpecificElement,
  clickOnSpecificElement,
  assertEqual,
  selectSpecificDropdownOption,
  waitForElementToBeVisible,
  count,
};
