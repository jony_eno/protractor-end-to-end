let SpecReporter = require("jasmine-spec-reporter").SpecReporter;
let TestConfigObj = require("./utilities/ExcelReader");
let AllureReporter = require("jasmine-allure-reporter");
let passCount = 0;
let failCount = 0;
let skipCount = 0;

const suiteType = TestConfigObj.getConfigurationValue("ConfigValue", "Suite Type");
const browserName = TestConfigObj.getConfigurationValue("ConfigValue", "Browser");
const macIpAddress = TestConfigObj.getConfigurationValue("ConfigValue", "Machine IP Address");
const port = TestConfigObj.getConfigurationValue("ConfigValue", "Port");
const gridExecution = TestConfigObj.getConfigurationValue("ConfigValue", "GridExecution");
var directConn = true;

if (gridExecution === "Yes") {
  directConn = false;
}

exports.config = {
  directConnect: directConn,
  seleniumAddress: "http://" + macIpAddress + ":" + port + "/wd/hub",

  suites: {
    suitesToExecute: "./tests/" + suiteType + "/*.js",
  },

  capabilities: {
    browserName: browserName.toLowerCase(),
  },

  onPrepare: function () {
    const fs = require('fs');
    let executionStatus = {
      totalPassed: passCount,
      tatalFailed: failCount,
    };
    let data = JSON.stringify(executionStatus);
    fs.writeFileSync('Output.json', data);

    browser.manage().window().maximize();

    // Following code is to take a screenshot on failure 
    var originalAddExpectationResult = jasmine.Spec.prototype.addExpectationResult;
    jasmine.Spec.prototype.addExpectationResult = function () {
      if (!arguments[0]) {
        browser.takeScreenshot().then(function (png) {
          allure.createAttachment(
            "Screenshot",
            function () {
              return new Buffer(png, "base64");
            },
            "image/png"
          )();
        });
      }
      return originalAddExpectationResult.apply(this, arguments);
    };

    // Following code will generate .xml reports into 'allure-results' directory
    jasmine.getEnv().addReporter(
      new AllureReporter({
        resultsDir: "allure-results",
      })
    );

    jasmine.getEnv().addReporter(
      new SpecReporter({
        displayStacktrace: "none", // display stacktrace for each failed assertion, values: (all|specs|summary|none)
        displaySuccessesSummary: true, // display summary of all successes after execution
        displayFailuresSummary: true, // display summary of all failures after execution
        displayPendingSummary: true, // display summary of all pending specs after execution
        displaySuccessfulSpec: true, // display each successful spec
        displayFailedSpec: true, // display each failed spec
        displayPendingSpec: true, // display each pending spec
        displaySpecDuration: true, // display each spec duration
        displaySuiteNumber: true, // display each suite number (hierarchical)
        colors: {
          success: "green",
          failure: "red",
          pending: "yellow",
        },
        prefixes: {
          success: "✓ ",
          failure: "✗ ",
          pending: "* ",
        },
        customProcessors: [],
      })
    );

    jasmine.getEnv().addReporter({
      specDone: function (result) {
        if (result.status == 'passed') {
          passCount++;
        }
        if (result.status == 'failed') {
          failCount++;
        }
        if (result.status == 'skipped') {
          skipCount++;
        }
      }
    });
  },

  onComplete: function () {
    const fs = require('fs');
    let executionStatus = {
      totalPassed: passCount,
      totalFailed: failCount,
      totalSkipped: skipCount,
    };
    let data = JSON.stringify(executionStatus);
    fs.writeFileSync('Output.json', data);
  }
};