var nodemailer = require('nodemailer');
let TestConfigObj = require("../utilities/ExcelReader");

const from = TestConfigObj.getConfigurationValue("ConfigValue", "From");
const to = TestConfigObj.getConfigurationValue("ConfigValue", "To");

const getCount = function () {
    let jsonData = require('../Output.json');
    var body2 = `Please Find the execution status below:<br>Total Passed: ${jsonData.totalPassed}.<br>Total Failed: ${jsonData.totalFailed}.<br>Total Skipped: ${jsonData.totalSkipped}.
    <br>Please don't reply, it's a automation generated email`;
    return body2;
}

const transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: from,
        pass: "sunday21"
    }
});

const mailOptions = {
    from: from,
    to: to,
    subject: 'Reporest Result',
    html: getCount(),
};

transport.sendMail(mailOptions, function (error, info) {
    if (error) {
        console.log("Email is not being sent");
    } else {
        console.log("Message sent: " + info.response);
        response.send(info);
    }
});