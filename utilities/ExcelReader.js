var fs = require("fs-extra");
const excelToJson = require("convert-excel-to-json");

const confData = excelToJson({
  source: fs.readFileSync("./testData/TestConfiguration.xls"),
  header: {
    rows: 1,
  },
});

const testData = excelToJson({
  source: fs.readFileSync("./testData/TestData.xls"),
  header: {
    rows: 1,
  },
});
const getTestData = (sheetName, keyword) => {
  return testData[sheetName].filter((item) => item.A === keyword)[0].B;
};

const getConfigurationValue = (sheetName, keyword) => {
  return confData[sheetName].filter((item) => item.B === keyword)[0].C;
};

module.exports = { getConfigurationValue, getTestData };
