** Installation/Prerequisites: You would need these before you jump into protractor world

    Java should be installed on your system.
    NodeJS, to install protractor you should have npm manager installed which comes by default with NodeJS

Install NodeJS from https://nodejs.org/. 

Note: Add nodeJS into your system environment variable.

** Setup:
1. Goto project root directory (e.g: ../Automation_Protractor).
2. Double click on 'EnvironmentSetUp.bat' batch file.
Now you are set to go.

** How to execute:
1. Goto "../Automation_Protector/testData" directory.
2. Open "testConfiguration.xls' file.
3. Give the informations that you want to command your framework to do during execution and hit the save button.
4. Now go back to the root directory and double click on the "execution_runner.bat" batch file.
5. Your execution will be started running within a few second.

** Automation Test Report:
1. After complition of the execution goto "../Automation_Protector/testResults" directory.
2. Here you will be able to find your report folder with the name format below:
allure-report_'system_current_date_time_stamp'  (e.g: allure-reports_2020-07-26_07-56-37)
3. Double click on the folder and open the "index.html' file using Firefox browser to see your automation test report.
If the report is not being loaded properly in the Firefox then follow the below steps to configure the browser:
--> Open Firefox and go to "about:config" url and then make "privacy.file_unique_origin" boolean value to false.